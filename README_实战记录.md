# 实际操作记录

1.克隆 dockerfile
```
yum install git
git clone https://gitee.com/funet8/docker-training.git
```
2.进入相应目录构建进行
```
cd docker-training/centos7
# ls
aliyun-epel.repo  aliyun-mirror.repo  Dockerfile  supervisord.conf
构建镜像:
docker build -t  funet8/centos:7.2 .
```
3.运行容器：
```
docker run -itd -p 60921:22 --name FCentos  funet8/centos:7.2

docker run -itd --name FCentos3 funet8/centos:7.2


# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                   NAMES
5da308c6f987        funet8/centos:7.2   "/usr/bin/supervisord"   11 minutes ago      Up 11 minutes       0.0.0.0:60921->22/tcp   FCentos
```
4.进入容器
```
# docker exec -it FCentos /bin/bash
[root@5da308c6f987 /]# 
```
## 一、构建mysql镜像和容器
```
cd docker-training/mysql/

构建mariadb镜像:
docker build -t  funet8/centos_mariadb .
启动镜像：
docker run -itd -p 61920:3306 --name Mariadb1 funet8/centos_mariadb
docker run -itd -p 61922:3306 --name Mariadb2 -v /data/mysql:/var/lib/mysql  funet8/centos_mariadb
进入容器：
docker exec -it Mariadb1 /bin/bash

新建mysql用户，允许远程登录
mysql>use mysql;
mysql>update user set password=PASSWORD('123456') where User='root1';
mysql>GRANT ALL PRIVILEGES ON *.* TO 'root1'@'%' IDENTIFIED BY '123456' WITH GRANT OPTION;
mysql>FLUSH PRIVILEGES;
```
## 二、构建nginx和php-frm镜像和容器
```
cd docker-training/php-fpm
git pull

docker build -t funet8/centos_nginx .

docker run -itd -p 80:80 --name nginx1 funet8/centos_nginx
```

## 三、构建wordpress站点容器
```
cd docker-training/wordpress/

docker build -t funet8/centos_wordpress .
docker run -itd -p 81:80 --name wordpress1 funet8/centos_nginx

遇到问题：
不能打开wordpress站点。

```






## docker网络问题
```
pkill docker 
iptables -t nat -F 
ifconfig docker0 down 
brctl delbr docker0 
docker -d 
service docker restart

-bash: brctl: command not found
解决：
yum install bridge-utils
```


